# Ansible Role: GitHub Desktop

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-github-desktop/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-github-desktop/-/commits/main)

This role installs [GitHub Desktop](https://github.com/shiftkey/desktop) client on any supported host.

## Requirements

None

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    github_desktop_version: latest # tag "release-2.9.5-linux1" if you want a specific version
    github_desktop_package_name: github-desktop
    github_desktop_repo_path: https://github.com/shiftkey/desktop/releases/download

This role can install the latest or a specific version. See [available GitHub Desktop releases](https://github.com/shiftkey/desktop/releases/) and change this variable accordingly.

    github_desktop_version: latest # tag "release-2.9.5-linux1" if you want a specific version

The name of the package installed in the system (if there need to be updated it).

    github_desktop_package_name: github-desktop

GitHub Desktop needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install GitHub Desktop. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: github-desktop

## License

MIT / BSD
